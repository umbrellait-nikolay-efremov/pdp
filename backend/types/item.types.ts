export enum ItemTypes {
    TRANSMISSION = 'transmission',
    SUSPENSION = 'suspension',
    BODY = 'body',
    ENGINE = 'engine',
}
